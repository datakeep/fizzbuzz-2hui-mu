/**
 *
 */
package jp.co.junit;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

/**
 * @author saito.kana
 *
 */
public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("setUp");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown");
	}

	/**
	 * {@link jp.co.junit.FizzBuzz#main(java.lang.String[])} のためのテスト・メソッド。
	 */
	@Test
	public void checkFizzBuzz_01() {
		System.out.println("checkFizzBuzz_01");
        assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	/**
	 * {@link jp.co.junit.FizzBuzz#main(java.lang.String[])} のためのテスト・メソッド。
	 */
	@Test
	public void checkFizzBuzz_02() {
		System.out.println("checkFizzBuzz_02");
        assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}

	/**
	 * {@link jp.co.junit.FizzBuzz#main(java.lang.String[])} のためのテスト・メソッド。
	 */
	@Test
	public void checkFizzBuzz_03() {
		System.out.println("checkFizzBuzz_03");
        assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}

	/**
	 * {@link jp.co.junit.FizzBuzz#main(java.lang.String[])} のためのテスト・メソッド。
	 */
	@Test
	public void checkFizzBuzz_04() {
		System.out.println("checkFizzBuzz_04");
        assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}

	/**
	 * {@link jp.co.junit.FizzBuzz#main(java.lang.String[])} のためのテスト・メソッド。
	 */
	@Test
	public void checkFizzBuzz_05() {
		System.out.println("checkFizzBuzz_05");
        assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}
}
