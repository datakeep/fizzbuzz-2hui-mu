package jp.co.junit;

public class FizzBuzz {
    public static void main(String[] args) {
        // 挙動の確認用に1〜100までの整数を引数にして「checkFizzBuzz」を呼び出す
        for(int i = 1; i <= 100; i++) {
            System.out.println(checkFizzBuzz(i));
        }
    }

    public static String checkFizzBuzz(int num) {
    	//3と5両方の倍数なら「FizzBuzz」
    	if((num % 3 == 0) && (num % 5 == 0)) {
		return "FizzBuzz";

    	//引数numが3の倍数なら「Fizz」
    	} else if(num % 3 == 0) {
    		return "Fizz";

    	//5の倍数なら「Buzz」
    	} else if(num % 5 == 0) {
    		return "Buzz";

    	//それ以外ならそのまま数字を戻り値として返す
    	} else {
    		return String.valueOf(num);
    	}
    }
}
